for tag in gene_GENCODE_V43 gene_GENCODE_V43_upstream200bp gene_GENCODE_V43_upstream500bp gene_GENCODE_V43_upstream2kb gene_GENCODE_V43_upstream10kb gene_GENCODE_V43_exons gene_GENCODE_V43_downstream2kb
do
grep -v pseudogene /mca_share/work/renardc_tmp/hg38/annot_2023/annotations_GENCODE_V43/${tag}.bed > ${tag}_noPseudo.bed
done
