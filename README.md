# tailor-annot

Custom annotations project for different types of sequencing data. The annoations include genomic positions, chromatin states, cancer driver genes and regulatory elements. The tables are based on a combination of ENCODE data.
